$(function(){
    if (document.location.hash){
        var hs = document.location.hash.slice(1).split('::');
        $('legend[@innerHTML='+hs[0]+']').before($(document.createElement('a')).attr("name",hs[0]));
        $('a[@innerHTML='+hs[0]+']').attr("name",hs[0]);
        if ($('a[@innerHTML='+hs[0]+']').parent().parent().hasClass("collapsed"))
            $('a[@innerHTML='+hs[0]+']').click();
        document.location.hash = "#"+hs[0];
        $('label[@innerHTML='+hs[1]+']').parent().parent().parent()[0].style.backgroundColor="yellow";
    }
})
